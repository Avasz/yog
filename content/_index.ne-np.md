+++
title = 'सामिसत्र'
date = 2024-04-19T21:57:25+05:45
draft = false
+++

{{< callout type="info" >}}
कृपया धैर्य गर्नुहोला!
{{< /callout >}}


{{< cards >}}
  {{< card link="docs/first_sem" title="पहिलो सामिसत्र" icon="academic-cap" >}}
  {{< card link="docs/second_sem" title="दोस्रो सामिसत्र" icon="x-circle" >}}
  {{< card link="docs/third_sem" title="तेस्रो सामिसत्र" icon="x-circle" >}}
  {{< card link="docs/fourth_sem" title="चौथो सामिसत्र" icon="x-circle" >}}
{{< /cards >}}

