+++
title = 'प्रयोगात्मक योग'
date = 2024-04-19T21:57:25+05:45
draft = false
+++

{{< cards >}}
  {{< card link="lectures" title="Lectures" icon="academic-cap" >}}
  {{< card link="homeworks" title="Homeworks" icon="x-circle" >}}
  {{< card link="downloads" title="Downloads" icon="x-circle" >}}
  {{< card link="others" title="Others" icon="x-circle" >}}
{{< /cards >}}



### क. प्रयोगात्मक योग


- __पाठ्यांश कोड__: MYSc.555
- __श्रेयोऽङ्क (Credit Hours)__: 3
- __पाठ्य घण्टा__: 48

### ख. उद्देश्य

यस पाठ्यांशको अध्ययनबाट विद्यार्थीहरु निम्नलिखित ज्ञान, सीप तथा क्षमताको विकास गर्न सक्षम हुनेछन्:

- षट्कर्मको सामान्य परिचय एवं योगभ्यासको पूर्वतयारी गर्न
- यौगिक सूक्ष्म तथा स्थूल व्यायाम; आसन, प्राणायाम, प्रत्याहार, मुद्रा एवं बन्धको प्रयोग गर्न
- सूर्यनमस्कारको अभ्यास गर्न
- धारणा र ध्यानको अभ्यास गराउन।

### ग. पाठ्यांशविवरणम्

#### १. एकाइ एक - शोधन एवं प्रार्थना

- श्रेयोऽङ्क: .७५
- पाठ्यसमय: १२ घण्टा

{{< details title="१.१ षट्कर्म" closed="true" >}}
  क. जलनेति,  
  ख. अग्निसार,  
  ग. वमनधौति,  
  घ. स्थल वस्ति,  
  ङ. बहिर्त्राटक,  
  च. कपालभाति (वातकर्म)  
{{< /details >}}

{{< details title="१.२ प्रार्थना" closed="true" >}}
  क. ओमकारं विन्दु संयुक्तम्...  
  ख. त्वमेव माता...  
  ग. आ ब्रह्मन् ब्राह्मणो...  
  घ. सर्वेषां स्वस्तिर्भवतु...  
{{< /details >}}


{{< details title="१.३ सूक्ष्मव्यायाम, गत्यात्मक व्यायाम र सूर्यनमस्कारासन" closed="true" >}}

  ###### १.३.१. सूक्ष्म व्यायाम

  - नेत्रशक्ति, शिर तथा गर्धनसक्ति विकासक क्रिया  
  - मेधाशक्ति, स्कन्ध तथा बाहुमूलशक्ति विकासक क्रिया  
  - वक्षस्थलशक्ति तथा कटिशक्ति विकाशक क्रिया  
  - चरणशक्ति विकासक क्रिया  

  ###### १.३.२. गत्यात्मक व्यायाम  
  - पादोत्तानासन क्रिया, पादसञ्चालनासन क्रिया, पादचक्रासन क्रिया  
  - गुडासन क्रिया, झुलासन क्रिया  
  - मेरुदण्ड अभ्यासात्मक क्रिया  
  - यौगिक जगिंग  
  - कार्यात्मक व्यायामहरू - जाँतो (चक्की) चालनासन क्रिया, काष्ठताक्षासन, नौकाचालन...  


  ###### १.३.३. सूर्यनमस्कार:   
  - १. प्रणामासन  
  - २. हस्त-उत्तानासन  
  - ३. पादहस्तासन  
  - ४. अश्वसञ्चालनासन (दायाँ गोडा अगाडि)  
  - ५. पर्वतासन  
  - ६. अष्टाङ्गनमस्कारासन  
  - ७. भुजङ्गासन  
  - ८. पर्वतासन  
  - ९. अश्वसञ्चालनासन  
  - १०. पादहस्तासन  
  - ११. हस्ता-उत्तानासन  
  - १२. प्राणामासन  
{{< /details >}}


#### २. एकाइ दुई - आसनाभ्यास

- श्रेयोऽङ्क: १.५
- पाठ्यसमय: २४ घण्टा

{{< details title="२.१ उठेर गरिने आसन: (८)" closed="true" >}}
  - ताडासन  
  - तिर्यक्ताडासन  
  - कटिचक्रासन  
  - उत्कटासन  
  - द्विकोणासन  
  - त्रिकोणासन  
  - ध्रुवासन  
  - वृक्षासन  
{{< /details >}}

{{< details title="२.२ बसेर गरिने आसन: (१३)" closed="true" >}}

  - सुखासन  
  - स्वस्तिकासन  
  - अर्धपद्मासन  
  - भद्रासन  
  - वज्रासन  
  - सिंहासन  
  - कागासन  
  - कूर्मासन  
  - वक्रासन  
  - गोमुखासन  
  - जानुशिरासन  
  - वीरासन  
  - शशाङ्कासन  
{{< /details >}}

{{< details title="२.३ घोप्टो सुतेर गरिने आसन:" closed="true" >}}
  - मकरासन  
  - भुजङ्गासन  
  - शलभासन  
  - नाभ्यासन (विपरीत नौकासन)  
  - धनुरासन  
  - बालशयनासन  
{{< /details >}}

{{< details title="२.४ उत्तानो सुतेर गरिने आसन:" closed="true" >}}
  - मर्कटासन  
  - उत्तानपादासन  
  - नाभिदर्शनासन  
  - सेतुबन्धासन (कन्धरासन)  
  - नौकासन  
  - पवनमुक्तासन  
  - सर्वाङ्गासन  
  - मत्स्यासन  
  - सुप्तवज्रासन  
  - शवासन  
{{< /details >}}


#### ३. प्राणायम, बन्ध, मुद्रा, प्रत्याहार र ध्यान

- श्रेयोऽङ्क: .७५
- पाठ्यसमय: १२ घण्टा

{{< details title="३.१ प्राणायम" closed="true" >}}
- सूर्यभेदन  
- उज्जायी  
- सीत्कारी  
- शीतली  
- भस्त्रिका  
- भ्रामरी  
- मूर्च्छा  
- प्लाविनी  
{{< /details >}}

{{< details title="३.२ मुद्रा तथा बन्ध" closed="true" >}}
###### ३.२.१ मुद्रा:
  - ज्ञान  
  - चिन  
  - आकाश  
  - वायु  
  - सूर्य  
  - वरुण  
  - पृथ्वी  
  - खेचरी  
  - अश्विनी  
  - तडागी मुद्रा  
###### ३.२.२ बन्ध:
- मूल बन्ध  
- जालन्धर बन्ध  
- उड्डियान बन्ध  
{{< /details >}}


{{< details title="३.३ प्रत्याहार, धारणा र ध्यान (प्रारम्भिक अभ्यास)" closed="true" >}}

###### ३.३.१ प्रत्याहार (योगनिद्रा) र धारणा (पञ्च धारणा)
###### ३.३.२ ध्यान
  - स्थूल ध्यरान  
  - सक्रिय ध्यान  
  - सहज ध्यान  
  - आनापाना
{{< /details >}}



