+++
title = 'किताबहरू'
date = 2024-04-19T21:57:25+05:45
draft = false
+++

{{< cards >}}
    {{< card link="http://www.shdvef.com/wp-content/uploads/2019/05/%E0%A4%B6%E0%A5%8D%E0%A4%B0%E0%A5%80-%E0%A4%AD%E0%A4%A4%E0%A5%83%E0%A4%B9%E0%A4%B0%E0%A4%BF-%E0%A4%A8%E0%A5%80%E0%A4%A4%E0%A4%BF-%E0%A4%B6%E0%A4%A4%E0%A4%95.pdf" title="भर्तृहरी नीतिशतकं" image="images/nitishatak.png" subtitle="Source: https:\/\/shdvef.com" >}}  
    {{< card link="http://www.shdvef.com/wp-content/uploads/2019/01/Chanakya-Neeti-Hindi.pdf" title="चाणक्यनीति" image="images/chanakyaniti.png" subtitle="Source: https:\/\/shdvef.com" >}}
    {{< card link="https://ia800101.us.archive.org/9/items/hitopadesha_hindi/hitopadesha_hindi.pdf" title="हितोपदेश" image="images/hitopadesh.png" subtitle="Source: https:\/\/archive.org" >}}
    
   
{{< /cards >}}





