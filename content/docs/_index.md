+++
title = 'Semesters'
date = 2024-04-19T21:57:25+05:45
draft = false
+++

{{< callout type="info" >}}
Please be patient!
{{< /callout >}}


{{< cards >}}
  {{< card link="first_sem" title="First Semester" icon="academic-cap" >}}
  {{< card link="second_sem" title="Second Semester" icon="academic-cap" >}}
  {{< card link="third_sem" title="Third Semester" icon="academic-cap" >}}
  {{< card link="fourth_sem" title="Fourth Semester" icon="academic-cap" >}}
{{< /cards >}}

