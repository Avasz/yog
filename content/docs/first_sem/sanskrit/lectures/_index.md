+++
title = 'Lectures'
date = 2024-04-19T21:57:25+05:45
draft = false
+++


### २. एकाइ द्वे - सुभाषितानि

#### १. [भर्तृहरि नीति शतक](unit_2/bhartrihari)
#### २. [हितोपदेश मित्रलाभ](unit_2/hitopadesh)
#### ३. [चाणक्यनीति](unit_2/chanakyaniti)


