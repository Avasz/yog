+++
title = 'Second Semester'
date = 2024-04-19T21:57:25+05:45
draft = false
weight = 2
+++

### पाठ्यक्रमको रूपरेखा
|पत्र (Paper)|विषय (Subject)|कोड (Code) | क्रेडिट आवर (Credit Hours) | पाठ्य घण्टा (Teaching Hours)|
|:--|:--|:--:|:--:|:--:|
|I|योग मनोविज्ञान एवं मानव चेतना (Yoga, Psychology & Human Consciousness)|MYSc.556|3|48|
|II|आयुर्वेद एवं प्राकृतिक चिकित्सा (Ayurveda and Naturopathy)|MYSc.557|3|48|
|III|आहार विज्ञान (Food Science)|MYSc.558|3|48|
|IV|नेपाली योग परम्पराको अध्ययन (Study of Nepalese Yoga Tradition)|MYSc.559|3|48|
|V|प्रयोगात्मक योग: विशिष्टीकृत योग अभ्यास (Yoga Practical: Advanced Yoga Practice)|MYSc.560|3|48|