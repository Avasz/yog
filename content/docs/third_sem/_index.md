+++
title = 'Third Semester'
date = 2024-04-19T21:57:25+05:45
draft = false
weight = 3
+++

### पाठ्यक्रमको रूपरेखा
|पत्र (Paper)|विषय (Subject)|कोड (Code) | क्रेडिट आवर (Credit Hours) | पाठ्य घण्टा (Teaching Hours)|
|:--|:--|:--:|:--:|:--:|
|I|पातञ्जल योगदर्शन (Fundamentals of Yoga)|MYSc.561|3|48|
|II|यौगिक जीवनशैली व्यवस्थापन (Yogic Lifestyle Management)|MYSc.562|3|48|
|III|भगवद्गीता र सांख्यकारिका (Bhagvat Gita & Sankhyakarika)|MYSc.563|3|48|
|IV|योग चिकित्सा (Yoga Therapy)|MYSc.564(A)|3|48|
|IV|आध्यात्मिक योग अध्ययन (Spiritual Yoga Study)|MYSc.564(B)|3|48|
|V|प्रयोगात्मक योग: योग चिकित्सा (Practical: Yoga Therapy)|MYSc.565(A)|3|48|
|V|प्रयोगात्मक योग: आध्यात्मिक योग (Practical: Spiritual Yoga)|MYSc.565(B)|3|48|