+++
title = 'Fourth Semester'
date = 2024-04-19T21:57:25+05:45
draft = false
weight = 4
+++




### पाठ्यक्रमको रूपरेखा
|पत्र (Paper)|विषय (Subject)|कोड (Code) | क्रेडिट आवर (Credit Hours) | पाठ्य घण्टा (Teaching Hours)|
|:--|:--|:--:|:--:|:--:|
|I|नाथयोग परम्परा र हठयोग|MYSc.568|3|48|
|II|प्रायोगिक योग: शिक्षा, स्वास्थ्य एवं पर्यटनमा योग  (Applied Yoga in Education, Health and Tourism)|MYSc.569|3|48|
|III|अनुसन्धान पद्धति (Research Methodology)|MYSc.570|3|48|
|IV|क्षेत्रगत कार्य: योग शिविर, योग भ्रमण र प्रतिवेदन लेखन   (Field Work: Yoga Camp, Tour & Report Writing)|MYSc.571|3|48|
|V|सोधपत्र लेखन   (Dessertation)|MYSc.572|6|96|