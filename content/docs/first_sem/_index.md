+++
title = 'First Semester'
date = 2024-04-19T21:57:25+05:45
draft = false
weight = 1
+++



{{< cards >}}
  {{< card link="sanskrit/" title="संस्कृत अध्ययन" icon="book-open" >}}
  {{< card link="prachya/" title="प्राच्य वाङ्गमयमा योग" icon="book-open" >}}
  {{< card link="anatomy/" title="मानव शरीर रचना तथा क्रिया विज्ञान" icon="eye" >}}
  {{< card link="" title="योगका आधारभूत सिद्धान्त" icon="book-open" >}}
  {{< card link="practical/" title="प्रयोगात्मक योग" icon="book-open" >}}
{{< /cards >}}



### पाठ्यक्रमको रूपरेखा
|पत्र (Paper)|विषय (Subject)|कोड (Code) | क्रेडिट आवर (Credit Hours) | पाठ्य घण्टा (Teaching Hours)|
|:--|:--|:--:|:--:|:--:|
|I|योगका आधारभूत तत्त्व (Fundamentals of Yoga)|MYSc.551|3|48|
|II|मानव शरीररचना तथा क्रिया विज्ञान (Human Anatomy and Physiology)|MYSc.552|3|48|
|III|संस्कृत अध्ययन (Sanskrit Study)|MYSc.553|3|48|
|IV|प्राच्य वाङ्गमयमा योग (Yoga in Ancient Literatures)|MYSc.554|3|48|
|V|प्रयोगात्मक योग: आधारभूत योग अभ्यास (Basic Yoga Practice)|MYSc.555|3|48|